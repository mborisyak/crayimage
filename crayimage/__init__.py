"""
  Developed primarily as a toolkit for data analysis in CRAYFIS experiment.
"""

__version__ = '0.1.0'
__author__ = 'CRAYFIS collaboration, Yandex School of Data Analysis and contributors.'

import warnings

from . import datautils

try:
  from .runutils import Run
except ImportError as e:
  warnings.warn(str(e))

try:
  from . import imgutils
except ImportError as e:
  warnings.warn(str(e))

try:
  from . import runutils
except ImportError as e:
  warnings.warn(str(e))

try:
  from . import statutils
except ImportError as e:
  warnings.warn(str(e))

try:
  from . import hotornot
except ImportError as e:
  warnings.warn(str(e))

from . import simulation

try:
  simulation.ensure_integral_spectra()
except:
  import traceback
  warnings.warn('Failed to generated spectra!')
  traceback.print_exc()
