import numpy as np
import array

import os
import os.path as osp

from ..datautils import get_dir, get_file

PARTICLES = ['e-', 'e+', 'gamma', 'mu-', 'mu+', 'proton']

DIFF_SPECTRA = 'diff_spectra'
BACKGROUND_SPECTRA = 'background_spectra'

def locate_diff_spectrum(particle):
  return get_file('data', DIFF_SPECTRA, particle + '.dat')

def get_spectrum(particle):
  path = locate_diff_spectrum(particle)
  datfile = np.loadtxt(path)

  ns = np.arange(datfile.shape[0] + 1)
  bins = 10.0 ** (ns / 10.0 - 2)
  assert np.allclose(datfile[:, 0], (bins[1:] + bins[:-1]) / 2.0, rtol=1.0e-2, atol=0.0)

  flux = datfile[:, 1] * (bins[1:] - bins[:-1])

  return bins, flux

def check_integral_spectra(particles=PARTICLES):
  try:
    spectra_dir = get_dir('data', BACKGROUND_SPECTRA)

    for particle in particles:
      if not osp.exists(osp.join(spectra_dir, particle + '.root')):
        return False

    return True
  except:
    return False

def generate_integral_spectra(particles=PARTICLES):
  import ROOT as r

  data_dir = get_dir('data')
  spectra_dir = osp.join(data_dir, BACKGROUND_SPECTRA)

  try:
    os.makedirs(spectra_dir)
  except:
    pass

  for particle in particles:
    f_out = r.TFile(osp.join(spectra_dir, particle + '.root'), 'RECREATE')

    bins, flux = get_spectrum(particle)

    binsx = array.array('d', bins)
    h = r.TH1F("energy", particle, len(binsx) - 1, binsx)
    for i, rate in enumerate(flux):
      h.Fill(
        (binsx[i] + binsx[i + 1]) / 2,
        flux[i]
      )

    h.Write()
    f_out.Close()

  return spectra_dir

def locate_integral_spectrum(particle):
  ensure_integral_spectra([particle])
  return get_file('data', BACKGROUND_SPECTRA, particle + '.root')

def ensure_integral_spectra(particles=PARTICLES):
  if not check_integral_spectra(particles):
    return generate_integral_spectra(particles)
  else:
    return get_dir('data', BACKGROUND_SPECTRA)

def get_total_flux(particle):
  _, flux = get_spectrum(particle)
  return np.sum(flux)

def get_priors(particles=PARTICLES):
  flux = dict()
  for particle in particles:
    particle_flux = get_total_flux(particle)
    flux[particle] = particle_flux

  total_flux = np.sum(list(flux.values()))

  return dict([
    (k, v / total_flux)
    for k, v in flux.items()
  ])

def locate_angle_distribution():
  return get_file('data', 'theta_cos2.root')