from ...datautils import get_file

def seed_stream(super_seed):
  import random
  random.seed(super_seed)

  used_seeds= set()

  while True:
    seed = (random.randrange(int(1.0e+6)), random.randrange(int(1.0e+6)))

    if seed in used_seeds:
      continue
    else:
      used_seeds.add(seed)
      yield seed

def get_seeds(n, super_seed):
  import random
  random.seed(super_seed)

  numbers = []

  while len(set(numbers)) != n:
    numbers = [
      (random.randrange(int(1.0e+6)), random.randrange(int(1.0e+6)))
      for _ in range(n)
    ]

  return numbers


get_config_path = lambda : get_file('config/run.mac.template')

def get_config_template(path='config/run.mac.template'):
  path = get_file(path)

  with open(path, 'r') as f:
    return f.read()

BINARY = 'TestEm1'

def get_binary():
  return BINARY
