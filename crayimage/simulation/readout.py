import numpy as np

from .track_utils import impose

__all__ = [
  'isotropic_decomposition',
  'ReadoutTransformation',
  'l1_sim',
  'ELECTRON_ENERGY',
  'MeV'
]

ELECTRON_ENERGY = 3.6 # eV
MeV = 1e+6 ### eV

def isotropic_decomposition(size):
  """
  Returns isotropic basis for conv filters of size `size x size`.
  """
  assert 2 * (size // 2) + 1 == size, 'only works for odd sizes'

  coordinates = [ (i, j) for i in range(size) for j in range(size) ]
  distances = [ (i - size // 2) ** 2 + (j - size // 2) ** 2 for i, j in coordinates ]

  from collections import defaultdict
  decomposition = defaultdict(lambda : list())

  for (i, j), d in zip(coordinates, distances):
    decomposition[d].append((i, j))

  decomposition = sorted(list(decomposition.items()), key = lambda x: x[0])

  def make_matrix(coords):
    m = np.zeros(shape=(size, size), dtype='float32')

    for x, y in coords:
      m[x, y] = 1.0

    return m

  return np.array([
    make_matrix(coords) for _, coords in decomposition
  ])


class ReadoutTransformation(object):
  def __init__(self, conv_size=3, monotonic_units=10, monotonic_alpha=5):
    import theano
    import theano.tensor as T

    self.conv_size = conv_size

    self.monotinic_units = monotonic_units

    self.conv_decomposition = isotropic_decomposition(conv_size)
    self.conv_basis = T.constant(
      self.conv_decomposition,
      name='isotropic_conv%dx%d_basis' % (conv_size, conv_size),
      dtype='float32'
    )

    self.conv_coefficients = T.fvector('isotropic conv coefs')
    self.conv_filter = T.sum(self.conv_basis * self.conv_coefficients[:, None, None], axis=0)

    self.images = T.ftensor4()
    self.convolved = T.nnet.conv2d_transpose(
      self.images,
      self.conv_filter.reshape((1, 1, conv_size, conv_size)),
      filter_shape=(1, 1, conv_size, conv_size),
      border_mode='half',
      output_shape=(None, 1, self.images.shape[2], self.images.shape[3])
    )

    self.monotonic_offsets = T.fvector()
    self.monotonic_alpha = T.constant(monotonic_alpha, dtype='float32')
    self.transformed_raw = T.mean(
      T.nnet.sigmoid(
        self.monotonic_alpha * (self.convolved[:, :, :, :, None] - self.monotonic_offsets[None, None, None, None, :])
      ),
      axis=4
    )

    self.monotonic_min = T.mean(T.nnet.sigmoid(
      -self.monotonic_alpha * self.monotonic_offsets
    ))

    self.monotonic_max = T.mean(T.nnet.sigmoid(
      self.monotonic_alpha * (1 - self.monotonic_offsets)
    ))

    self.transformed = (self.transformed_raw - self.monotonic_min) / (self.monotonic_max - self.monotonic_min)

    xs = T.fvector()
    transformed_xs_raw = T.mean(
      T.nnet.sigmoid(
        self.monotonic_alpha * (xs[:, None] - self.monotonic_offsets[None, :])
      ),
      axis=1
    )

    transformed_xs = (transformed_xs_raw - self.monotonic_min) / (self.monotonic_max - self.monotonic_min)


    self._f = theano.function(
      inputs=[self.images, self.conv_coefficients, self.monotonic_offsets],
      outputs=self.transformed,
      allow_input_downcast=True,
    )

    self._f_test = theano.function(
      inputs=[xs, self.monotonic_offsets],
      outputs=transformed_xs,
      allow_input_downcast=True,
    )

  def __call__(self, images, conv_coefs, offsets, batch_size=None):
    if batch_size is None:
      return self._f(images, conv_coefs, offsets)
    else:
      n = images.shape[0]
      return np.vstack([
        self._f(images[i * batch_size:(i + 1) * batch_size], conv_coefs, offsets)
        for i in range(n // batch_size + (0 if n % batch_size == 0 else 1))
      ])

def l1_sim(
  n_samples,
  l1_pass_window, calibration_windows,
  camera_size, target_image_size,
  noise_level,
  particle_flux, particle_efficiencies,  tracks,
  readout, filter_coefs, offsets
):
  """
  Simulates results of l1 trigger.

  :param n_samples: number of samples to return.
  :param l1_pass_window: pass only one in `l1_pass_window` frames.
  :param calibration_windows: number of calibration windows to estimate threshold.
  :param camera_size: size of the camera in pixels.
  :param target_image_size: size of returned images.
  :param noise_level: lambda for Poisson process, in electrons.
  :param particle_flux: flux for each particles, in 1 / pix^2.
  :param particle_efficiencies: probabilities of interacting with camera for each particle.
  :param tracks: particle name -> IndexedSparseImages
  :param readout: instance of `ReadoutTransformation`
  :param filter_coefs: the first param for readout, coefficients for isotropic filter decomposition.
  :param offsets: the second param for readout, specifies positions of sigmoid for the
    energy -> camera output transformation.
  :return: list of tuples:
    (original GEANT image, processed image, theta, phi, incident_energy, particle type)
  """

  def get_calib_image():
    image = np.random.poisson(lam=noise_level, size=camera_size) * ELECTRON_ENERGY
    image = image.astype('float32')

    for particle in particle_flux:
      number_of_hits = np.random.poisson(lam=particle_flux[particle] * camera_size[0] * camera_size[1])
      efficiency = particle_efficiencies[particle]

      for hit_i in range(number_of_hits):
        if np.random.uniform() < efficiency:
          track_indx = np.random.choice(tracks[particle].size())
          xs, ys, vals = tracks[particle].track(track_indx)

          vals *= MeV

          cx = np.random.randint(0, camera_size[0])
          cy = np.random.randint(0, camera_size[1])

          impose(xs, ys, vals, image, cx, cy)

    processed = readout(
      image.reshape((1, 1,) + image.shape),
      filter_coefs,
      offsets
    )

    return processed

  def get_target_images(num):
    """
    Instead of forming a larger image and then slicing it,
    this function directly forms smaller images in an approximately the same way.
    """
    images = []

    ### assuming that particles never cross each other
    for particle in particle_flux:
      number_of_hits = np.random.poisson(lam=particle_flux[particle] * camera_size[0] * camera_size[1])

      efficiency = particle_efficiencies[particle]

      for hit_i in range(number_of_hits):
        blank = np.zeros(shape=target_image_size, dtype='float32')

        image = np.random.poisson(lam=noise_level, size=target_image_size) * ELECTRON_ENERGY
        image = image.astype('float32')

        if np.random.uniform() < efficiency:
          track_indx = np.random.choice(tracks[particle].size())
          xs, ys, vals = tracks[particle].track(track_indx)
          vals *= MeV

          theta = tracks[particle].theta[track_indx]
          phi = tracks[particle].theta[track_indx]
          incident_energy = tracks[particle].incident_energy[track_indx]

          impose(xs, ys, vals, image, 0, 0)
          impose(xs, ys, vals, blank, 0, 0)
        else:
          theta = 0
          phi = 0
          incident_energy = 0

        processed = readout(
          image.reshape((1, 1,) + image.shape),
          filter_coefs,
          offsets
        )

        images.append((blank, processed, theta, phi, incident_energy, particle))

    ### filling the rest of the images with noise
    while len(images) < num:
      image = np.random.poisson(lam=noise_level, size=target_image_size) * ELECTRON_ENERGY
      image = image.astype('float32')

      blank = np.zeros(shape=target_image_size, dtype='float32')

      theta = 0
      phi = 0
      incident_energy = 0

      processed = readout(
        image.reshape((1, 1,) + image.shape),
        filter_coefs,
        offsets
      )

      images.append((blank, processed, theta, phi, incident_energy, None))

    return images


  ### calibration step
  n_calib = l1_pass_window * calibration_windows

  max_brightness = np.zeros(n_calib)

  for calib_i in range(n_calib):
    image = get_calib_image()
    max_brightness[calib_i] = np.max(image)

  l1_threshold = np.percentile(max_brightness, q=(1 - 1 / l1_pass_window) * 100)

  target_images_in_frame = int(
    (camera_size[0] / target_image_size[0]) * (camera_size[1] / target_image_size[1])
  )
  target_images_in_frame = min(target_images_in_frame, 1)
  selected = []

  while len(selected) < n_samples:
    proposals = get_target_images(target_images_in_frame)

    for proposal in proposals:
      if np.max(proposal[1]) >= l1_threshold:
        selected.append(proposal)

  return selected

