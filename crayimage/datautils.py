import os
import os.path as osp

__all__ = [
  'PACKAGE_ROOT',
  'get_resource',
  'get_dir',
  'get_file',
  'get_script'
]

def get_package_root():
  path = osp.abspath(osp.dirname(__file__))
  return path

PACKAGE_ROOT = get_package_root()

def get_resource(paths, dir=True):
  resourse = osp.abspath(osp.join(PACKAGE_ROOT, *paths))

  assert osp.exists(resourse) and (osp.isdir if dir else osp.isfile)(resourse), \
    'resourse {what} [{where}] does not seem like a {what}'.format(
      where = resourse, what = 'directory' if dir else 'file'
    )

  return resourse

get_dir = lambda *paths: get_resource(paths, dir=True)
get_file = lambda *paths: get_resource(paths, dir=False)

get_script = lambda script_name: get_file('scripts', script_name + '.py')